import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by IlyaMakarov on 12/15/2016.
 */
public class RmsysTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp(){
        driver = new FirefoxDriver();
    }
    @AfterClass
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void testLogin() {
        driver.get("https://192.168.100.26/");
        WebElement userName = driver.findElement(By.id("Username"));
        userName.sendKeys("IlyaMakarov");
        WebElement password = driver.findElement(By.id("Password"));
        password.sendKeys("IlyaMakarov");

//        WebElement userNameById = driver.findElement(By.id("Username"));
//        WebElement userNameByLinText = driver.findElement(By.linkText("SomeText"));
//        WebElement userNameByClassName = driver.findElement(By.className("SomeText"));
//        WebElement userNameByCssSelector = driver.findElement(By.cssSelector("SomeText"));
//        WebElement userNameByName = driver.findElement(By.name("SomeText"));
//        WebElement userNameByPartialLinkText = driver.findElement(By.partialLinkText("SomeText"));
//        WebElement userNameByIdTagName = driver.findElement(By.tagName("SomeText"));
//        WebElement userNameByIdXPath = driver.findElement(By.xpath(".//*[@id='Username']"));

        WebElement submitButton = driver.findElement(By.id("SubmitButton"));
        submitButton.click();

        WebElement signOut = driver.findElement(By.linkText("Sign Out"));
        Assert.assertNotNull(signOut);
    }
}
